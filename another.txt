complementary = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'}
def reverse_complement(seq):
    seq = seq.upper()
    reverse_complementary = "".join(complementary.get(base, base) for base in reversed(seq))
    print("Reverse complementary:"+reverse_complementary)
